import DefaultLayout from '~/layouts/Default.vue'
import Vue from 'vue';

export default function (Vue, { head }) {
  Vue.component('Layout', DefaultLayout);
}

Vue.prototype.$eventBus = new Vue();