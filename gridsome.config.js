const path = require('path')

function addStyleResource (rule) {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [
        path.resolve(__dirname, './src/assets/scss/style.scss'),
      ],
    })
}



module.exports = {
  siteName: 'Basche liefert',
  siteDescription: 'eine Ansammlung von Händlern aus der Umgebung, die bereit sind, zu Ihnen nach Hause zu liefern. Sie unterstützen damit den regionalen Einzelhandel und tragen dazu bei, dass auch in Zukunft diese Geschäfte erhalten bleiben',

  templates: {
    WordPressCategory: '/category/:slug', // adds a route for the "category" post type (Optional)
    WordPressPost: '/:year/:month/:day/:slug', // adds a route for the "post" post type (Optional)
    WordPressPostTag: '/tag/:slug' // adds a route for the "post_tag" post type (Optional)
  },

  plugins: [
    {
      use: '@gridsome/source-wordpress',
      options: {
        baseUrl: 'http://wpforgridsome.sayhejo.de/', // required
        typeName: 'WordPress', // GraphQL schema name (Optional)
      }
    }
  ],

  chainWebpack (config) {
    // Load variables for all vue-files
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal']

    // or if you use scss
    types.forEach(type => {
      addStyleResource(config.module.rule('scss').oneOf(type))
    })
	}
}
